***This is a repository where I keep my zshrc, i3, polybar, i3blocks (not in use anymore) and various scripts that they use.***

![alt text](https://i.imgur.com/1VxyLgy.png)
![alt text](https://i.imgur.com/gERmtaf.png)

* Wallpaper: wallpapers/26.jpg, wallpapers/33.jpg
* WM: i3-gaps
* OS: Arch Linux
* Bar: Polybar
* Shell: zsh + oh-my-zsh
* Icons: Font awesome 5
* Colors: pywal