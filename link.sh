#!/usr/bin/zsh
#####
# File: /home/sarange/.config/i3/link.sh
# Project: /home/sarange/.config/i3
# Created Date: Sunday, June 16th 2019, 8:49:49 pm
# Author: sarange
# -----
# Last Modified: Mon Jun 24 2019
# Modified By: sarange
# -----
# Copyright (c) 2019 sarange
# 
# Talk is cheap. Show me the code.
#####

ln -s ~/.config/i3/zshrc ~/.zshrc
ln ~/.cache/wal/colors-konsole.colorscheme ~/.local/share/konsole
xrdb -load ~/.config/i3/Xresources